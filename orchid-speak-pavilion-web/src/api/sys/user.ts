import { defHttp } from '/@/utils/http/axios'
import {
  LoginParams,
  registerParams,
  LoginResultModel,
  userInfoModelSuccess,
} from './model/userModel'

import { ErrorMessageMode } from '/#/axios'
import { ContentTypeEnum } from '/@/enums/httpEnum'

enum Api {
  Login = '/user/login',
  Register = '/user/register',
  Logout = '/user/logOut',
  GetUserInfo = '/user/userInfo',
}

/**
 * @description: user login api
 */
export function loginApi(params: LoginParams, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<LoginResultModel>(
    {
      url: Api.Login,
      params,
      headers: {
        'Content-Type': ContentTypeEnum.FORM_URLENCODED,
      },
    },
    {
      errorMessageMode: mode,
    },
  )
}
export function registerApi(params: registerParams, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.Register,
      params,
    },
    {
      errorMessageMode: mode,
    },
  )
}

/**
 * @description: getUserInfo
 */
export function getUserInfo() {
  return defHttp.get<userInfoModelSuccess>({ url: Api.GetUserInfo }, { errorMessageMode: 'none' })
}

export function doLogout() {
  return defHttp.post({ url: Api.Logout })
}
