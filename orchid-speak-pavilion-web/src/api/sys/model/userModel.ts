import { resultSuccess } from '../../model/baseModel'
import { SexEnum } from '/@/enums/sexEnum'

/**
 * @description: Login interface parameters
 */
export interface LoginParams {
  userName: string
  password: string
}

export interface formParams {
  userName?: String
  phoneNumber?: string
  sex?: SexEnum
  height?: string
  age?: Number
  weight?: string
  email?: string
}

/**
 * @description: register interface parameters
 */
export interface registerParams extends formParams {
  password: string
  confirmPassword: string
  policy: boolean
}

export interface RoleInfo {
  roleName: string
  value: string
}

/**
 * @description: Login interface return value
 */
export interface LoginResultModel {
  data: {
    token: string
    userId: string
  }
}

/**
 * @description: Get user information return value
 */
export interface GetUserInfoModel extends formParams {
  roles: RoleInfo[]
  // 用户id
  userId: string | number
  // 用户名
  username: string
  // 真实名字
  realName: string
  // 头像
  avatar: string
  // 介绍
  desc?: string

  points?: Number
}

export interface userInfoModelSuccess extends resultSuccess {
  data: GetUserInfoModel
}
