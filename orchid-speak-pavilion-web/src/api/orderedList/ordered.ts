import { defHttp } from '/@/utils/http/axios'

import { ErrorMessageMode } from '/#/axios'
import { orderedListModelSuccess } from './model/orderedModel'

enum Api {
  list = '/statistics/top3',
}

export function getOrderedList(_mode: ErrorMessageMode = 'modal') {
  return defHttp.get<orderedListModelSuccess>({ url: Api.list }, { errorMessageMode: 'none' })
}
