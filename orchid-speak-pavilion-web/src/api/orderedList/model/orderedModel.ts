import { resultSuccess } from '../../model/baseModel'

export interface orderedObjModel {
  userName: String
  points: Number
}

export interface orderedListModelSuccess extends resultSuccess {
  data: Array<orderedObjModel>
}
