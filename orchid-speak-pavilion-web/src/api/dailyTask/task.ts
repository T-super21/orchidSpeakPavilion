import { defHttp } from '/@/utils/http/axios'

import { lineTaskModel, pieTaskModel } from './model/taskModel'

enum Api {
  statisticsTasks = '/statistics/statisticsTasks',
  statisticsDetails = '/statistics/details',
  updataTask = '/tasks/updateCurrentValue',
}

interface taskParams {
  taskId: Number
}
interface updataTaskParams {
  id: Number | String
  currentValue: Number | String
}

export function statisticsTask(params: taskParams) {
  return defHttp.get<lineTaskModel>(
    { url: Api.statisticsTasks, params },
    { errorMessageMode: 'none' },
  )
}
export function statisticsDetailsTask(params: taskParams) {
  return defHttp.get<pieTaskModel>(
    { url: Api.statisticsDetails, params },
    { errorMessageMode: 'none' },
  )
}
export function updataTask(params: updataTaskParams) {
  return defHttp.put({ url: Api.updataTask, params }, { errorMessageMode: 'none' })
}
