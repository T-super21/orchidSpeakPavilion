import { resultSuccess } from '../../model/baseModel'

export interface lineTaskObjModel {
  timeData: Array<String>
  valueData: Array<Number>
  unit: String
}

export interface pieTaskObjModel {
  currentValue: Number
  targetValue: Number
  unit: String
}

export interface lineTaskModel extends resultSuccess {
  data: lineTaskObjModel
}

export interface pieTaskModel extends resultSuccess {
  data: pieTaskObjModel
}
