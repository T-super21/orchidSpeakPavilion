import { ResultEnum } from '/@/enums/httpEnum'

export interface BasicPageParams {
  page: number
  pageSize: number
}

export interface BasicFetchResult<T> {
  items: T[]
  total: number
}

export interface resultSuccess {
  code?: ResultEnum.SUCCESS
  message?: String
}
