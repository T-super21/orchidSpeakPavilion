export const cardList = (() => {
  const result: any[] = []
  for (let i = 0; i < 6; i++) {
    result.push({
      id: i,
      title: 'Orchid Speak Pavilion Management System',
      description: '健康描述',
      datetime: '2020-11-26 17:39',
      extra: '编辑',
      icon: 'logos:vue',
      color: '#1890ff',
      author: 'hmAdmin',
      percent: 20 * (i + 1),
    })
  }
  return result
})()
