export default {
  basic: {
    accountCenter: '个人中心',
  },
  userInfo: {
    veryThoughtful: '很有想法的',
    specializedDesign: '专注设计',
    sichuanGirl: '川妹子',
    longLegs: '大长腿',
    hainaBaiChuan: '海纳百川',
    frontEndDeveloper: '前端开发',
    vue3: 'Vue3',

    scienceBricklayingTeam: '科学搬砖组',
    middleClassTeenagers: '中二少年团',
    highImpactDesign: '高逼格设计',
    programmersDailyLife: '程序员日常',
    scienceBricklayers: '科学搬砖组',
  },
}
