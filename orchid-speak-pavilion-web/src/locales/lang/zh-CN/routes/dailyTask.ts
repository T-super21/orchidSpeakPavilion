export default {
  basic: {
    dailyTask: '每日任务',
    completeUpdata: '更新成功',
  },
  pages: {
    sleep: '睡眠',
    step: '步数',
    exerciseTimes: '运动时间',
    drink: '喝水',
  },
  liquidFillPanel: {
    updatedDrinkingTargetValues: '更新喝水目标值',
    originalTargetValue: '原目标值',
    targetValue: '目标值',
  },
  piePanel: {
    sleepTime: '睡眠时间',
    timeLeft: '剩余时间',
  },
}
