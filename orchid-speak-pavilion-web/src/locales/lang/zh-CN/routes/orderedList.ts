export default {
  basic: {
    orderedList: '排行榜',
  },
  list: {
    champion: '冠军',
    secondPlace: '亚军',
    thirdPlace: '季军',
  },
}
