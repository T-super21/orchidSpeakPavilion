export default {
  basic: {
    accountCenter: 'Account Center',
  },
  userInfo: {
    veryThoughtful: 'Very Thoughtful',
    specializedDesign: 'Specialized Design',
    sichuanGirl: 'Sichuan Girl',
    longLegs: 'Long Legs',
    hainaBaiChuan: 'HainaBaiChuan',
    frontEndDeveloper: 'Front-end Developer',
    vue3: 'Vue3',

    scienceBricklayingTeam: 'Science Bricklaying Team',
    middleClassTeenagers: 'Middle Class Teenagers',
    highImpactDesign: 'High-impact Design',
    programmersDailyLife: 'Programmers Daily Life',
    scienceBricklayers: 'Science Bricklayers',
  },
}
