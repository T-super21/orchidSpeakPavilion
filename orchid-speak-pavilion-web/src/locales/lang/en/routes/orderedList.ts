export default {
  basic: {
    orderedList: 'Ordered List',
  },
  list: {
    champion: 'Champion',
    secondPlace: 'Second Place',
    thirdPlace: 'Third Place',
  },
}
