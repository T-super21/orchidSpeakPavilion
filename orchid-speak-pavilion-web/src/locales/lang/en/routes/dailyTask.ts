export default {
  basic: {
    dailyTask: 'Daily Task',
    completeUpdata: 'Complete UpData',
  },
  pages: {
    sleep: 'Sleep',
    step: 'Step',
    exerciseTimes: 'Exercise Times',
    drink: 'Drink',
  },
  liquidFillPanel: {
    updatedDrinkingTargetValues: 'Updated Drinking Target Values',
    originalTargetValue: 'Original Target Value',
    targetValue: 'Target Value',
  },
  piePanel: {
    sleepTime: 'Sleep Time',
    timeLeft: 'Time Left',
  },
}
