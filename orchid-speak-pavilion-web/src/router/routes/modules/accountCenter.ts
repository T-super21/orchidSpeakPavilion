import type { AppRouteModule } from '/@/router/types'

import { LAYOUT } from '/@/router/constant'
import { t } from '../../../hooks/web/useI18n'

const accountCenter: AppRouteModule = {
  path: '/accountCenter',
  name: 'AccountCenter',
  component: LAYOUT,
  redirect: '/accountCenter/index',
  meta: {
    orderNo: 3,
    hideChildrenInMenu: true,
    icon: 'material-symbols:account-circle-full',
    title: t('routes.accountCenter.basic.accountCenter'),
  },
  children: [
    {
      path: 'index',
      name: 'AccountCenterPage',
      component: () => import('/@/views/accountCenter/index.vue'),
      meta: {
        title: t('routes.accountCenter.basic.accountCenter'),
        icon: 'material-symbols:account-circle-full',
      },
    },
  ],
}

export default accountCenter
