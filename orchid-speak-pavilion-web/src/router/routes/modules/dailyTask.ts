import type { AppRouteModule } from '/@/router/types'

import { LAYOUT } from '/@/router/constant'
import { t } from '../../../hooks/web/useI18n'

const dailyTask: AppRouteModule = {
  path: '/dailyTask',
  name: 'DailyTask',
  component: LAYOUT,
  redirect: '/dailyTask/index',
  meta: {
    orderNo: 1,
    hideChildrenInMenu: true,
    icon: 'fluent:task-list-square-ltr-20-regular',
    title: t('routes.dailyTask.basic.dailyTask'),
  },
  children: [
    {
      path: 'index',
      name: 'DailyTaskPage',
      component: () => import('/@/views/dailyTask/index.vue'),
      meta: {
        affix: true,
        title: t('routes.dailyTask.basic.dailyTask'),
        icon: 'fluent:task-list-square-rtl-24-filled',
      },
    },
  ],
}

export default dailyTask
