import type { AppRouteModule } from '/@/router/types'

import { LAYOUT } from '/@/router/constant'
import { t } from '../../../hooks/web/useI18n'

const orderedList: AppRouteModule = {
  path: '/orderedList',
  name: 'OrderedList',
  component: LAYOUT,
  redirect: '/orderedList/index',
  meta: {
    orderNo: 2,
    hideChildrenInMenu: true,
    icon: 'material-symbols:leaderboard-outline-rounded',
    title: t('routes.orderedList.basic.orderedList'),
  },
  children: [
    {
      path: 'index',
      name: 'OrderedListPage',
      component: () => import('/@/views/orderedList/index.vue'),
      meta: {
        title: t('routes.orderedList.basic.orderedList'),
        icon: 'material-symbols:leaderboard-outline-rounded',
      },
    },
  ],
}

export default orderedList
