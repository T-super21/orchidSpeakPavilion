import { MockMethod } from 'vite-plugin-mock'
import { resultError, resultSuccess, getRequestToken, requestParams } from '../_util'

export function createFakeUserList() {
  return [
    {
      userId: '1',
      userName: 'dogboy',
      realName: 'HM Admin',
      avatar:
        'https://img1.baidu.com/it/u=193696007,2125345610&fm=253&fmt=auto&app=120&f=JPEG?w=500&h=500',
      desc: 'manager',
      password: '2222',
      token: 'fakeToken1',
      homePath: '/dailyTask/index',
      roles: [
        {
          roleName: 'Super Admin',
          value: 'super',
        },
      ],
    },
    {
      userId: '2',
      userName: 'test',
      password: '123456',
      realName: 'test user',
      avatar: 'https://q1.qlogo.cn/g?b=qq&nk=339449197&s=640',
      desc: 'tester',
      token: 'fakeToken2',
      homePath: '/dashboard/workbench',
      roles: [
        {
          roleName: 'Tester',
          value: 'test',
        },
      ],
    },
  ]
}

const fakeCodeList: any = {
  '1': ['1000', '3000', '5000'],

  '2': ['2000', '4000', '6000'],
}
export default [
  // mock user login
  {
    url: '/user/login',
    timeout: 200,
    method: 'post',
    response: ({ body }) => {
      const { userName, password } = body
      const checkUser = createFakeUserList().find(
        (item) => item.userName === userName && password === item.password,
      )
      if (!checkUser) {
        return resultError('Incorrect account or password！')
      }
      const { userName: _username, token } = checkUser
      return resultSuccess({
        data: token,
      })
    },
  },
  {
    url: '/user/getUserInfo',
    method: 'get',
    response: (request: requestParams) => {
      const token = getRequestToken(request)
      if (!token) return resultError('Invalid token')
      const checkUser = createFakeUserList().find((item) => item.token === token)
      if (!checkUser) {
        return resultError('The corresponding user information was not obtained!')
      }
      return resultSuccess(checkUser)
    },
  },
  {
    url: '/basic-api/getPermCode',
    timeout: 200,
    method: 'get',
    response: (request: requestParams) => {
      const token = getRequestToken(request)
      if (!token) return resultError('Invalid token')
      const checkUser = createFakeUserList().find((item) => item.token === token)
      if (!checkUser) {
        return resultError('Invalid token!')
      }
      const codeList = fakeCodeList[checkUser.userId]

      return resultSuccess(codeList)
    },
  },
  {
    url: '/user/logOut',
    timeout: 200,
    method: 'post',
    response: (request: requestParams) => {
      const token = getRequestToken(request)
      if (!token) return resultError('Invalid token')
      const checkUser = createFakeUserList().find((item) => item.token === token)
      if (!checkUser) {
        return resultError('Invalid token!')
      }
      return resultSuccess(undefined, { message: 'Token has been destroyed' })
    },
  },
  {
    url: '/user/register',
    statusCode: 200,
    method: 'post',
    response: ({ body }) => {
      const { userName, phoneNumber } = body
      return resultSuccess({
        phoneNumber,
        userName,
      })
    },
  },
  {
    url: '/basic-api/testRetry',
    statusCode: 405,
    method: 'get',
    response: () => {
      return resultError('Error!')
    },
  },
  {
    url: '/statistics/statisticsTasks',
    method: 'get',
    response: (request: requestParams) => {
      const { taskId } = request.query
      return resultSuccess(
        taskId == '9'
          ? {
              timeData: ['00:00:21', '00:05:39', '00:05:46', '15:09:12'],
              valueData: [4000, 1000, 100, 100],
              unit: 'steps',
            }
          : {
              timeData: ['00:00:21', '00:05:39', '00:05:46', '15:09:12'],
              valueData: [20, 30, 60, 50],
              unit: 'minutes',
            },
      )
    },
  },
  {
    url: '/statistics/details',
    method: 'get',
    response: (request: requestParams) => {
      const { taskId } = request.query
      return resultSuccess(
        taskId == '10'
          ? {
              currentValue: 965,
              targetValue: 1930,
              unit: 'ml',
            }
          : {
              currentValue: 5,
              targetValue: 9,
              unit: 'hours',
            },
      )
    },
  },
  {
    url: '/statistics/top3',
    method: 'get',
    response: () => {
      return resultSuccess([
        {
          userName: 'dogboy47',
          points: 4,
        },
        {
          userName: 'dogboy41',
          points: 2,
        },
        {
          userName: 'dogboy43',
          points: 1,
        },
      ])
    },
  },
  {
    url: '/user/userInfo',
    method: 'get',
    response: () => {
      return resultSuccess({
        userName: 'dogboy47',
        phoneNumber: '11111',
        email: '111111',
        age: 24,
        sex: 'male',
        height: 180.0,
        weight: 91.0,
        points: 4,
      })
    },
  },
  {
    url: '/tasks/updateCurrentValue',
    method: 'put',
    response: () => {
      return resultSuccess({
        id: 10,
        userId: 22,
        taskTypeId: 2,
        taskName: 'Walking Goal',
        currentValue: 1000,
        targetValue: 9943,
        unit: 'steps',
        completed: false,
        date: '2024-07-27',
        custom: false,
      })
    },
  },
] as MockMethod[]
