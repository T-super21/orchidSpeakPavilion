const { default: Notify } = require("@vant/weapp/notify/notify");
import { httpRequest } from "../../utils/network/network";

const app = getApp();

import Toast from "@vant/weapp/toast/toast";
import { API } from "../../utils/network/api";

var num = 0;


Page({

  /**
   * 页面的初始数据
   */
  data: {
    loginStatus: app.globalData.loginStatus,
    emptyAvatar: app.globalData.emptyAvatar,
    show: false,
    userInfo: app.globalData.userInfo,
    showShare: false,
    version: app.globalData.version,
    updateVisivle: false,
    options: [
      { name: '微信', icon: 'wechat', openType: 'share' }
      // { name: '微博', icon: 'weibo' },
      // { name: '复制链接', icon: 'link' },
      // { name: '分享海报', icon: 'poster' },
      // { name: '二维码', icon: 'qrcode' },
    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getUserInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.setData({
      loginStatus: app.globalData.loginStatus
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    this.getUserInfo();
    setTimeout(() => {
      wx.stopPullDownRefresh();
    }, 500);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  onClick() {
    this.setData({ showShare: true });
  },

  onClose() {
    this.setData({ showShare: false });
  },

  onSelect(event) {
    Toast(event.detail.name);
    this.onClose();
  },
  toMyRelations(e) {
    if (!app.checkoutLoginStatis(this, e.type)) return;
    wx.navigateTo({
      url: "/pages/myRelations/myRelations"
    })
  },
  toAbout() {
    wx.navigateTo({
      url: "/pages/about/about",
    })
  },
  changeShow() {
    this.setData({
      show: !this.data.show
    })
  },
  async logout() {
    await httpRequest(API.logout);
    Notify({
      duration: app.globalData.notifyDuration,
      type: "success",
      message: "退出登录成功",
      onClose: () => {
        let domain = wx.getStorageSync("domain");
        // 清空登录状态
        wx.clearStorageSync();
        let loginStatus = false;
        app.globalData.loginStatus = loginStatus;

        wx.setStorageSync("loginStatus", loginStatus);
        wx.setStorageSync("domain", domain);

        this.setData({
          userInfo: {},
          loginStatus: loginStatus
        })
      }
    })
  },
  cancelLogin(e) {
    this.setData({
      show: e.detail.show
    })
  },
  async getUserInfo() {
    const res = await httpRequest(API.userInfo);
    this.setData({
      userInfo: res.data
    })
    app.globalData.userInfo = res.data;
  },
  // update userInfo success
  async successUpdate(e) {
    Toast.success("更新成功");
    await this.getUserInfo();
    this.setData({
      updateVisivle: false
    })
  },
  changeVisivle(e) {
    if (!app.checkoutLoginStatis(this, e.type)) return;
    this.setData({
      updateVisivle: true
    })
  },
  async successLogin(e) {
    await this.getUserInfo();
    this.setData({
      loginStatus: wx.getStorageSync("loginStatus"),
      show: false
    })
  },
  toLottie() {
    num++;
    if (num <= 10) return;
    wx.navigateTo({
      url: "/Lottie/pages/index/index",
    })
  }
})