import {
  httpRequest, showContent
} from "../../utils/network/network";

const app = getApp();

import Notify from "@vant/weapp/notify/notify";
import { API } from "../../utils/network/api";
import { calculateDaysDifferenceUtils } from "../../utils/index";


Page({

  /**
   * 页面的初始数据
   */
  data: {
    loginStatus: app.globalData.loginStatus,
    relationRadio: app.globalData.relationRadio,
    emptyAvatar: app.globalData.emptyAvatar,
    relationsList: [],
    partnerId: "", // share user id
    userName: "",
    sheetShow: false,
    show: false,
    confirmVisibile: false,
    rType: "LOVERS",
    generalDict: {
      RELATIONSHIP: {
        LOVERS: "情侣",
        BROTHER: "兄弟",
        SISTER: "姐妹"
      }
    },
    pageNum: 1,
    pageSize: 10,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      partnerId: options?.partnerId,
      rType: options?.rType || "LOVERS",
      userName: options?.userName
    })
    // 检查是否登录
    if (!app.checkoutLoginStatis(this, "click")) return;

    if (this.data.partnerId) {
      this.changeConfirmVisible();
    }
    this.getRelationsList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    if (!this.data.loginStatus) {
      this.setData({
        loginStatus: app.globalData.loginStatus
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    this.data.pageNum = 1;
    this.data.pageSize = 10;
    this.data.relationsList = []; // 清空
    this.getRelationsList();
    setTimeout(() => {
      wx.stopPullDownRefresh();
    }, 500);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage(e) {
    return {
      title: `和我建立${this.data.generalDict["RELATIONSHIP"][this.data.rType]}关系`,
      path: `pages/myRelations/myRelations?partnerId=${wx.getStorageSync("userId")}&rType=${this.data.rType}&userName=${app.globalData.userInfo.userName || "微信用户"}`
    }
  },
  /**
   * 建立关系
   * @param {*} partnerId 用户id
   * @param {*} rType 关系
   */
  createRelations(partnerId, rType) {
    if (wx.getStorageSync("userId") == partnerId) {
      showContent("建立关系失败,id不能相同");
      return;
    }
    let _this = this;
    httpRequest(API.relationshipAdd, {
      userId: wx.getStorageSync("userId"),
      partnerId,
      rType
    }, "POST", "application/json").then(() => {
      Notify({
        duration: app.globalData.notifyDuration,
        type: "success",
        message: `建立${_this.data.generalDict["RELATIONSHIP"][rType]}成功`,
        onClose: () => {
          _this.data.relationsList = [];
          _this.data.pageNum = 1;
          _this.data.pageSize = 10;
          _this.getRelationsList();
          _this.changeConfirmVisible();
        }
      })
    }).catch(err => {
      wx.showModal({
        title: "建立关系失败",
        content: `${err.message}:${err.code}`,
        showCancel: false
      })
    })
  },
  changeRelationsModel() {
    this.setData({
      sheetShow: !this.data.sheetShow
    })
  },
  // 获取当前关系
  async getRelationsList() {
    if (!this.data.loginStatus) {
      return;
    }
    let params = {
      pageNum: this.data.pageNum,
      pageSize: this.data.pageSize
    }
    let relationsList = this.data.relationsList;
    const res = await httpRequest(API.relationshipList, params);
    if (res.data.recordedList.length) {
      res.data.recordedList.forEach(item => {
        item._date = calculateDaysDifferenceUtils(item.createTime.split(" ")[0]);

      })
    }
    relationsList.push(...res.data.recordedList);
    this.setData({
      relationsList
    })
  },
  gotoLogin() {
    wx.navigateTo({
      url: "/pages/login/login",
    })
  },
  confirmAddRelationship() {
    this.createRelations(this.data.partnerId, this.data.rType);
  },
  changeConfirmVisible() {
    this.setData({
      confirmVisibile: !this.data.confirmVisibile
    })
  },
  cancelLogin(e) {
    this.setData({
      show: e.detail.show
    })
  },
  cancelAddRelation() {
    this.data.partnerId = "";
    this.data.userName = "";
    this.data.rType = "LOVERS";
    this.changeConfirmVisible();
  },
  successLogin(e) {
    this.setData({
      loginStatus: true
    })
    if (this.data.partnerId) {
      this.changeConfirmVisible();
    }
    this.data.pageNum = 1;
    this.data.pageSize = 10;
    this.data.relationsList = [];
    this.getRelationsList();
  }
})