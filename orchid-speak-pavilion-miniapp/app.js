import Notify from "@vant/weapp/notify/notify";

App({
  onLaunch(e) {
    // init app
    this.initApp();
  },
  globalData: {
    env: "",
    appName: "兰语阁",
    version: "V2.01.06",
    notifyDuration: 1500,// 提示显示时长
    loginStatus: wx.getStorageSync("loginStatus") || false, // 用户登录状态
    userInfo: {},
    emptyAvatar: "https://mmbiz.qpic.cn/mmbiz/icTdbqWNOwNRna42FI242Lcia07jQodd2FJGIYQfG0LAJGFxM4FbnQP6yfMxBgJ0F3YRqJCJ1aPAK2dQagdusBZg/0",
    emptyBgCalceldar: {
      COUNTDOWN: "https://img2.baidu.com/it/u=2048174210,4156512660&fm=253&fmt=auto&app=138&f=JPEG",
      MEMORIAL: "https://img0.baidu.com/it/u=1160089487,3786481384&fm=253&fmt=auto&app=138&f=JPEG"
    },
    relationRadio: [
      {
        name: "LOVERS",
        value: "情侣"
      },
      {
        name: "BROTHER",
        value: "兄弟"
      },
      {
        name: "SISTER",
        value: "姐妹"
      }
    ],
    statusRadio: [
      {
        name: "NOT_COMPLETE",
        value: "未完成"
      },
      {
        name: "COMPLETE",
        value: "已完成"
      }
    ],
    calendarRadio: [
      {
        name: "COUNTDOWN",
        value: "倒计时"
      },
      {
        name: "MEMORIAL",
        value: "纪念日"
      }
    ]
  },
  initApp() {
    var domain = "http://127.0.0.1";
    var port = 3000;
    const config = wx.getAccountInfoSync();
    switch (config.miniProgram.envVersion) {
      case "develop":
        // domain = "https://192.168.31.251";
        // domain = "https://172.20.10.6";
        // domain = "https://127.0.0.1";
        domain = "https://www.ittgh.cn";
        break;
      case "trial":
        domain = "https://www.ittgh.cn";
        break;
      case "release":
        domain = "https://www.ittgh.cn";
        console.log = () => { };
        break;
      default:
        domain = "http://127.0.0.1";
    }
    this.globalData.env = config.miniProgram.envVersion;
    let url = `${domain}:${port}`;
    this.globalData.domain = url;
    wx.setStorageSync("domain", url);

    // login status
    !wx.getStorageSync("loginStatus") ? wx.setStorageSync("loginStatus", false) : "";
  },
  checkoutLoginStatis(_this) {
    // click type && logout
    if ((arguments[1] == "click" || arguments[1] == "tap") && !wx.getStorageSync("loginStatus")) {
      _this.setData({
        show: !wx.getStorageSync("loginStatus")
      })
    }
    _this.setData({
      loginStatus: wx.getStorageSync("loginStatus"),
    })
    return _this.data.loginStatus;
  },
  // get general dict
  async getGeneralDict(_this) {
    const dict = {};
    for (const key in _this.data.generalDict) {
      dict[key] = await getGeneralDictByType(key);
      _this.setData({
        generalDict: dict
      })
    }
  },
  showNoAuthMessage(name = "功能") {
    Notify({
      duration: this.globalData.notifyDuration,
      type: "warning",
      message: `该${name}不归您所有,不支持当前操作～`
    })
  },
  showNotify(message = "", type = "warning") {
    Notify({
      duration: this.globalData.notifyDuration,
      type,
      message
    })
  }
})
