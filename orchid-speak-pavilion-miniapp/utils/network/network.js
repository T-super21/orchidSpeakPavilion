var app = null;
/**
 * 网络请求
 * 
 */
export const httpRequest = (url, params = {}, method = "GET", type) => {
  app = getApp();
  wx.showLoading({
    title: "请求中...",
  })
  return new Promise((resolve, reject) => {
    if (!url) {
      showContent("请求url不能为空");
      reject("请求url不能为空");
      wx.hideLoading();
      return;
    }
    const domain = app.globalData.domain || wx.getStorageSync("domain");
    wx.request({
      url: `${domain}${url}`,
      data: params,
      method,
      header: {
        "Authorization": wx.getStorageSync("token") || "",
        "platformType": "MINIAPP",
        "content-type": type ? type : method == "POST" ? "application/x-www-form-urlencoded" : "application/json"
      },
      success: res => {
        // request success code
        if (res.data.code == 0) {
          resolve(res.data);
        } else {
          // request fail code
          res.statusCode == 401 ? showContent({ code: res.statusCode, message: "token过期,请重新登录～" }) : showContent(res.data);
        }
      },
      fail: err => {
        showContent(err);
        reject(err);
      },
      complete: () => {
        wx.hideLoading();
      }
    })
  })
}

export const showContent = (message = {}) => {
  const title = "温馨提示";
  if (Object.prototype.toString.call(message) == "[object Object]") {
    message.title = title;
    if (message.errMsg) {
      wx.showModal({
        title: message.title,
        content: message.errMsg,
        showCancel: false,
      })
      return;
    }
    // Authorization fail
    if (message.code == 401) {
      wx.setStorageSync("loginStatus", false);
      wx.setStorageSync("token", null);
      wx.setStorageSync("userId", null);
      let pages = getCurrentPages();
      let indexPage = pages[0];
      app.showNotify(message.message); // 全局notify
      wx.switchTab({
        url: `/${indexPage.route}`,
        success: () => {
          // 全局登录
          indexPage.setData({
            show: true
          })
        },
        fail: err => {
          console.log(err);
        }
      })
      // wx.showModal({
      //   title: message.message,
      //   content: `${message.message};状态码:${message.code}`,
      //   showCancel: false,
      //   success: res => {
      //     // 重新登录
      //     wx.setStorageSync("loginStatus", false);
      //     wx.setStorageSync("token", null);
      //     let pages = getCurrentPages();
      //     let indexPage = pages[0];
      //     wx.switchTab({
      //       url: '/pages/index/index',
      //       success: () => {
      //         indexPage.setData({
      //           show: true
      //         })
      //       }
      //     })
      //   }
      // })
      return;
    } else if (message.code == 1) {
      wx.showModal({
        title,
        content: `${message.message};状态码:${message.code}`,
        showCancel: false,
      })
    }
  } else if (Object.prototype.toString.call(message) == "[object String]") {
    wx.showModal({
      title,
      content: message,
      showCancel: false,
    })
    return;
  } else {
    wx.showModal({
      title,
      content: message.toString(),
      showCancel: false,
    })
    return;
  }


}