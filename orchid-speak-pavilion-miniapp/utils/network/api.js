const web = "/api/web";
const version = "/v1";
const prefix = `${web}${version}`;

const API = {
  // user api
  login: "/user/login",
  logout: "/user/logout",
  userInfo: "/user/userInfo",
  updateUserInfo: "/user/update",

  // file api
  upload: "/files/upload", // local upload
  tencentUpload: "/tencent/upload",

  // love list api
  loveList: "/loveLists",
  addLoveList: "/loveLists/add",
  updateLoveList: "/loveLists/update",
  getLoveList: "/loveLists/list",

  // calendar list 
  calendar: "/calendar",
  calendarList: "/calendar/list",
  calendarAdd: "/calendar/add",
  calendarUpdate: "/calendar/update",

  // relation list
  relationship: "/relationship",
  relationshipList: "/relationship/list",
  relationshipAdd: "/relationship/add",

  // general dict
  generalDict: "/dict/general",
}
for (let key in API) {
  API[key] = `${prefix}${API[key]}`
}

module.exports = {
  API
}