<div align="center"> 
<a href="https://gitee.com/T-super21/orchidSpeakPavilion"> <img alt="orchidSpeakPavilion Logo" width="215" src="https://gitee.com/T-super21/loveSpeakPavilion/raw/master/images/lanyuge_logo.png"> </a> 
</div>
<h1 align="center">Welcome to 兰语阁 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.0.01-blue.svg?cacheSeconds=2592000" />
  <a href="https://www.yuque.com/tangyi-5ldnl/rzv4xd/nl8d6g6mobdgylk7?singleDoc#" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="#" target="_blank">
    <img alt="License: MulanPSL-2.0" src="https://img.shields.io/badge/License-MulanPSL2.0-yellow.svg" />
  </a>
</p>

## 项目简介

基于 **SpringBoot**、**Vue3**、**uni-app** 构建高效、方便、简洁的情侣恋爱管理平台，专为便捷与美学设计，精细权限管理，涵盖专属情侣空间、共享相册、共享重要节日倒计时提醒、日常小事分享、相互打卡纪念、专属回忆等核心功能。

## 功能演示

### 前台 & 后台

| 平台 | 访问链接                             | 账号密码                          
| ---- | ---------------------------------- | --------------------------------|
| 前台 | [暂不开放]()                         | 账号：,密码：                     |
| 后台 | [暂不开放]()                         | 默认管理员账号：，密码：             |

### 小程序体验

<img src="https://gitee.com/T-super21/loveSpeakPavilion/raw/master/images/miniAppCode.jpg" width="200"/>

## 功能概述

<img src="https://gitee.com/T-super21/loveSpeakPavilion/raw/1.0/projectImages/service.png" width="600"/>

本项目综合 后台管理系统与 微信小程序，旨在打造流畅的在线情侣分享和恋爱环境。

- **后台管理系统**：聚焦在线管理，提供管理员使用，可对用户模块、恋爱清单模块、日历模块和共享相册模块的同意管理，适应不同的用户体验。
- **微信小程序**：精简而不简单，同样专注于情侣之间的体验，确保用户随时随地的了解自己的另一半，增进彼此的感情。

## 项目架构

### 技术选型
- [Vue3](https://v3.vuejs.org/)：要熟悉Vue3
- [Es6+](http://es6.ruanyifeng.com/) - 要熟悉es6
- [Element UI](https://element-plus.org/zh-CN/#/zh-CN)：Element3 UI库
- [Node](https://nodejs.org/)：Node 环境 ≥ v20.13.1


- [IDEA](https://www.jetbrains.com/zh-cn/idea/download/other.html)：IDEA ≥ v2023.2.2
- [Maven](https://maven.apache.org/)：Maven ≥ v19
- [SpringBoot]()：Spring Boot >= 2
- [Mybatis]()：Mybatis ≥ 3.5.9
- [JDK]()：JDK ≥ 21


### 主要目录说明
```
orchidSpeakPavilion
└───orchid-speak-pavilion-service              后端代码
└───orchid-speak-pavilion-miniapp               小程序代码
└───orchid-speak-pavilion-web                  管理平台代码
```


## 功能截图

<table>
    <tr>
        <td><img src="https://gitee.com/T-super21/loveSpeakPavilion/raw/1.0/projectImages/login.png" height="200"/></td>
        <td><img src="https://gitee.com/T-super21/loveSpeakPavilion/raw/1.0/projectImages/index.png" height="200"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/T-super21/loveSpeakPavilion/raw/1.0/projectImages/loveList.png" height="200"/></td>
        <td><img src="https://gitee.com/T-super21/loveSpeakPavilion/raw/1.0/projectImages/calendar.png" height="200"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/T-super21/loveSpeakPavilion/raw/1.0/projectImages/user.png" height="200"/></td>
        <td>
            <img src="https://gitee.com/T-super21/loveSpeakPavilion/raw/1.0/projectImages/miniLogin.png" height="200">
            <img src="https://gitee.com/T-super21/loveSpeakPavilion/raw/1.0/projectImages/miniCalendar.png" height="200">
            <img src="https://gitee.com/T-super21/loveSpeakPavilion/raw/1.0/projectImages/miniRelation.png" height="200">
        </td>
    </tr>
</table>

## 交流

欢迎提交 PR、[issues](https://gitee.com/T-super21/loveSpeakPavilion)一起完善项目。

👤 **作者**

- Gitee: [@T-super21](https://gitee.com/T-super21)

### QQ交流群

<img src="https://gitee.com/T-super21/loveSpeakPavilion/raw/master/images/ercode.png" width="130"/>


### 请作者喝咖啡

如果您觉得有帮助，请点右上角 ⭐️ "Star" 或者**微信扫一扫**支持一下，谢谢！

<img src="https://gitee.com/T-super21/loveSpeakPavilion/raw/1.0/projectImages/code.png" width="130"/>

## 许可证

本项目采用 [MulanPSL-2.0](http://license.coscl.org.cn/MulanPSL2) 授权。