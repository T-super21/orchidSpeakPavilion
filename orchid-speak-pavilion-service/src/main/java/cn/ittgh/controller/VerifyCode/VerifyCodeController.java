package cn.ittgh.controller.VerifyCode;

import cn.ittgh.pojo.Commons.Result;
import cn.ittgh.pojo.VerifyCode.VerifyImgCode;
import cn.ittgh.service.VerifyCode.VerifyImgCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/verify")
public class VerifyCodeController {

    @Autowired
    private VerifyImgCodeService verifyImgCodeService;

    @GetMapping("/img")
    public Result<VerifyImgCode> generateValidateImgCode() {
        return Result.success(verifyImgCodeService.generateVerifyImgCode());
    }
}
