package cn.ittgh.controller.LoveLists;

import cn.ittgh.pojo.Commons.PageBean;
import cn.ittgh.pojo.Commons.Result;
import cn.ittgh.pojo.LoveLists.LoveLists;
import cn.ittgh.service.LoveLists.LoveListsService;
import cn.ittgh.utils.EnumsUtil;
import cn.ittgh.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/loveLists")
public class LoveListsController {

	@Autowired
	private LoveListsService loveListsService;

	@PostMapping("/add")
	public Result addLoveLists(@RequestBody @Validated LoveLists loveLists) {
		loveListsService.addLoveLists(loveLists);
		return Result.success();
	}

	@DeleteMapping("/{id}")
	public Result deleteLoveLists(
			@PathVariable("id") String id
	) {
		loveListsService.removeLoveLists(id);
		return Result.success();
	}

	@PostMapping("/update")
	public Result updateLoveLists(
			@RequestBody @Validated LoveLists loveLists
	) {
		loveListsService.updateLoveLists(loveLists);
		return Result.success();
	}

	@GetMapping("/{id}")
	public Result getLoveListsInfo(@PathVariable("id") String id) {
		LoveLists loveLists = loveListsService.getLoveListsInfo(id);
		return Result.success(loveLists);
	}

	@GetMapping("/list")
	public Result<PageBean<LoveLists>> getAllLoveLists(
			Integer pageNum,
			Integer pageSize,
			EnumsUtil.LoveListStatus status
	) {
		String userId = ThreadLocalUtil.getUserId();
		PageBean<LoveLists> loveLists = status != null ? loveListsService.getLoveListsById(pageNum, pageSize, userId, status) : loveListsService.getAllLoveLists(pageNum, pageSize);
		return Result.success(loveLists);
	}
}
