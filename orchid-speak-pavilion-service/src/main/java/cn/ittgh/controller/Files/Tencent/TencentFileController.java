package cn.ittgh.controller.Files.Tencent;

import cn.ittgh.pojo.Commons.Result;
import cn.ittgh.service.Files.TencentFiles.TencentFilesService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/tencent")
public class TencentFileController {
	@Autowired
	private TencentFilesService tencentFilesService;

	@Value("${files.tencent.maxSize}")
	private long maxSize;

	@PostMapping("/upload")
	public Result upload(
			HttpServletRequest request,
			@RequestParam(value = "file") MultipartFile file
	) {
		if (file.getSize() <= 0) {
			Result.error("上传的文件大小不能为空");
		}
		if (file.getSize() > maxSize * 1024 * 1024) {
			Result.error("上传的文件大小不能超过20MB");
		}
		if (!Objects.requireNonNull(file.getContentType()).startsWith("image")) {
			Result.error("上传的文件不是图片类型");
		}
		return Result.success(tencentFilesService.uploadFiles(file));
	}

	@DeleteMapping("/deleteImage")
	public Result deleteImageFile(@RequestParam("id") String id) {
		tencentFilesService.deleteImage(id);
		return Result.success();
	}
}
