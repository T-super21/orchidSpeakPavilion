package cn.ittgh.controller.Dictionary;

import cn.ittgh.pojo.Commons.Result;
import cn.ittgh.pojo.Dictionary.GeneralDict.Manager.Manager;
import jakarta.validation.constraints.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/dict")
public class DictionaryController {

	private final Manager dictManager = new Manager();

	@GetMapping("/general")
	private <T> Result<T> getGeneralDict(@RequestParam @NotNull String type) {
		return (Result<T>) Result.success(dictManager.getDictionaryByType(type));
	}
}
