package cn.ittgh.controller.Calendar;

import cn.ittgh.pojo.Commons.PageBean;
import cn.ittgh.pojo.Commons.Result;
import cn.ittgh.pojo.Calendar.Calendar;
import cn.ittgh.service.Calendar.CalendarService;
import cn.ittgh.utils.EnumsUtil;
import cn.ittgh.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/calendar")
public class CalendarController {

    @Autowired
    private CalendarService calendarService;

    @PostMapping("/add")
    public Result addCalendar(@RequestBody @Validated Calendar calendar) {
        calendarService.addCalendar(calendar);
        return Result.success();
    }

    @DeleteMapping("/{id}")
    public Result deleteCalendar(
            @PathVariable("id") String id
    ) {
        calendarService.removeCalendar(id);
        return Result.success();
    }

    @PostMapping("/update")
    public Result updateCalendar(
            @RequestBody @Validated Calendar calendar
    ) {
        calendarService.updateCalendar(calendar);
        return Result.success();
    }

    @GetMapping("/{id}")
    public Result getCalendarInfo(@PathVariable("id") String id) {
        Calendar calendar = calendarService.getCalendarInfo(id);
        return Result.success(calendar);
    }

    @GetMapping("/list")
    public Result<PageBean<Calendar>> getAllCalendar(
            Integer pageNum,
            Integer pageSize
    ) {
        String userId = ThreadLocalUtil.getUserId();
        PageBean<Calendar> calendar = calendarService.getCalendarById(pageNum, pageSize, userId);
        return Result.success(calendar);
    }
}
