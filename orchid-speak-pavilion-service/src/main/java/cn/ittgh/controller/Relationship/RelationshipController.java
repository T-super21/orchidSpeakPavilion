package cn.ittgh.controller.Relationship;

import cn.ittgh.pojo.Relationship.MyRelationship;
import cn.ittgh.pojo.Relationship.Relationship;
import cn.ittgh.pojo.Commons.PageBean;
import cn.ittgh.pojo.Commons.Result;
import cn.ittgh.service.Relationship.RelationshipService;
import cn.ittgh.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/relationship")
public class RelationshipController {

	@Autowired
	private RelationshipService relationshipService;

	@PostMapping("/add")
	public Result addRelationship(@RequestBody @Validated Relationship relationship) {
		List<Relationship> relationshipList = relationshipService.getRelationshipByIdAndType(relationship.getUserId(), relationship.getPartnerId(), relationship.getRType());
		if (!relationshipList.isEmpty()) {
			return Result.error("数据库已存在该记录");
		}
		relationshipService.addRelationship(relationship);
		return Result.success();
	}

	@DeleteMapping("/{id}")
	public Result deleteRelationship(
			@PathVariable("id") String id
	) {
		relationshipService.removeRelationship(id);
		return Result.success();
	}

	@PostMapping("/update")
	public Result updateRelationship(
			@RequestBody @Validated Relationship relationship
	) {
		relationshipService.updateRelationship(relationship);
		return Result.success();
	}

	@GetMapping("/{id}")
	public Result getRelationshipInfo(@PathVariable("id") String id) {
		Relationship relationship = relationshipService.getRelationshipInfo(id);
		return Result.success(relationship);
	}

	@GetMapping("/list")
	public Result<PageBean<MyRelationship>> getAllRelationship(
			Integer pageNum,
			Integer pageSize
	) {
		String userId = ThreadLocalUtil.getUserId();
		PageBean<MyRelationship> myRelationship = relationshipService.getRelationshipById(pageNum, pageSize, userId);
		return Result.success(myRelationship);
	}
}
