package cn.ittgh.utils;

import java.util.Map;

public class ThreadLocalUtil {
	private static final ThreadLocal THREAD_LOCAL = new ThreadLocal();

	// 根据键获取值
	public static <T> T get() {
		return (T) THREAD_LOCAL.get();
	}

	// 存储键值对
	public static void set(Object value) {
		THREAD_LOCAL.set(value);
	}

	// 清除ThreadLocal 防止内存泄露
	public static void remove() {
		THREAD_LOCAL.remove();
	}

	public static String getUserId() {
		Map<String, Object> map = get();
		return (String) map.get("id");
	}
}
