package cn.ittgh.utils.Tencent;

public class TencentUtil {
	public Integer[] unzipPolyline(Integer[] polyLine) {
		for (var i = 2; i < polyLine.length; i++) {
			polyLine[i] = polyLine[i - 2] + polyLine[i] / 1000000;
		}
		return polyLine;
	}
}
