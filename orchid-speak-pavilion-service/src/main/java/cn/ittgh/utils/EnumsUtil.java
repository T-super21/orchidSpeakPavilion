package cn.ittgh.utils;

public class EnumsUtil {
	public enum Sex {
		MALE,
		FEMALE
	}

	public enum UserStatus {
		NORMAL,
		DISABLE
	}
	public enum LoveListStatus{
		COMPLETE,
		NOT_COMPLETE
	}

	public enum PlatformType {
		MINIAPP,
		WEB
	}

	public enum Roles {
		VISITOR,
		GENERAL_USER,
		MANAGER,
		SUPER_MANAGER
	}

	public enum FORUM_TAGS {
		MAKE_FRIENDS,
		SEEK_FOR_HELP,
		FEEDBACK,
	}
	public enum CALENDAR_TYPE{
		COUNTDOWN,
		MEMORIAL
	}
	public enum RELATIONSHIP{
		LOVERS,
		BROTHER,
		SISTER
	}

}
