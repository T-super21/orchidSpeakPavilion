package cn.ittgh.mapper.LoveLists;

import cn.ittgh.pojo.LoveLists.LoveLists;
import cn.ittgh.utils.EnumsUtil;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface LoveListsMapper {

	@Insert("insert into tb_love_lists(user_id,title,cover,substance,remark,visible_range)" +
			" values(#{userId},#{title},#{cover},#{substance},#{remark},#{visibleRange})"
	)
	Boolean add(LoveLists loveLists);

	@Delete("delete from tb_love_lists where id = #{id}")
	Boolean deleteById(String id);

	Boolean updateLoveLists(LoveLists loveLists);

	@Select("select * from tb_love_lists")
	List<LoveLists> getAllLoveLists();

	List<LoveLists> getLoveListsById(String userId, EnumsUtil.LoveListStatus status);

	@Select("select * from tb_love_lists where id = #{id}")
	LoveLists getLoveListsInfoById(String id);

}
