package cn.ittgh.mapper.User;

import cn.ittgh.pojo.User.User;
import cn.ittgh.utils.EnumsUtil;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface UserMapper {
	@Select("select * from tb_users where user_name=#{userName} and platform_type=#{platformType}")
	User findByUserNameAndPlatformType(String userName, EnumsUtil.PlatformType platformType);

	@Select("select * from tb_users where phone_number=#{phoneNumber}")
	User findByPhoneNumber(String phoneNumber);

	@Update("<script>" +
			"update tb_users" +
			"<set>" +
			"<if test='userName != null'>" +
			"user_name=#{userName}," +
			"</if>" +
			"<if test='password !=null'>" +
			"password=#{password}," +
			"</if>" +
			"<if test='realName !=null'>" +
			"real_name=#{realName}," +
			"</if>" +
			"<if test='avatar !=null'>" +
			"avatar=#{avatar}," +
			"</if>" +
			"<if test='phoneNumber !=null'>" +
			"phone_number=#{phoneNumber}," +
			"</if>" +
			"<if test='sex !=null'>" +
			"sex=#{sex}," +
			"</if>" +
			"<if test='birthday !=null'>" +
			"birthday=#{birthday}," +
			"</if>" +
			"<if test='age !=null'>" +
			"age=#{age}," +
			"</if>" +
			"<if test='email !=null'>" +
			"email=#{email}," +
			"</if>" +
			"<if test='isVip !=null'>" +
			"is_vip=#{isVip}," +
			"</if>" +
			"<if test='homePath !=null'>" +
			"home_path=#{homePath}," +
			"</if>" +
			"<if test='status !=null'>" +
			"status=#{status}," +
			"</if>" +
			"<if test='loginStatus !=null'>" +
			"login_status=#{loginStatus}," +
			"</if>" +
			"<if test='remark !=null'>" +
			"remark=#{remark}," +
			"</if>" +
			"<if test='roles !=null'>" +
			"roles=#{roles}," +
			"</if>" +
			"<if test='platformType !=null'>" +
			"platform_type=#{platformType}," +
			"</if>" +
			"<if test='createTime !=null'>" +
			"create_time=#{createTime}," +
			"</if>" +
			"<if test='updateTime !=null'>" +
			"update_time=#{updateTime}," +
			"</if>" +
			"<if test='lastLoginTime !=null'>" +
			"last_login_time=#{lastLoginTime}," +
			"</if>" +
			"</set>" +
			"where id=#{id}" +
			"</script>"
	)
	Boolean updateUser(User user);


	@Insert(
			"insert into tb_users(user_name,password,phone_number,roles,platform_type,create_time)" +
					" values(#{userName},#{password},#{phoneNumber},#{roles},#{platformType},#{createTime})"
	)
	Boolean add(User user);

	@Select("select * from tb_users where id=#{id}")
	User findByUserId(String id);

	List<User> list(EnumsUtil.Roles roles, EnumsUtil.UserStatus status, EnumsUtil.PlatformType platformType);

	@Select("select * from tb_users")
	List<User> listAll();
}
