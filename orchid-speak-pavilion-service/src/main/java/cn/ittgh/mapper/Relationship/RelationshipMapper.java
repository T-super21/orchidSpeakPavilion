package cn.ittgh.mapper.Relationship;

import cn.ittgh.pojo.Relationship.MyRelationship;
import cn.ittgh.pojo.Relationship.Relationship;
import cn.ittgh.utils.EnumsUtil;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RelationshipMapper {

	@Insert("insert into tb_relationship(user_id,partner_id,r_type,remark)" +
			" values(#{userId},#{partnerId},#{rType},#{remark})"
	)
	Boolean add(Relationship calendar);

	@Delete("delete from tb_relationship where id = #{id}")
	Boolean deleteById(String id);

	Boolean updateRelationship(Relationship calendar);

	@Select("select * from tb_relationship where" +
			" (user_id = #{userId} and partner_id = #{partnerId} and r_type = #{rType})" +
			" or" +
			" (user_id = #{partnerId} and partner_id = #{userId} and r_type = #{rType})")
	List<Relationship> getRelationshipByIdAndType(String userId, String partnerId, EnumsUtil.RELATIONSHIP rType);

	@Select("select * from tb_relationship")
	List<MyRelationship> getAllRelationship();

	List<MyRelationship> getRelationshipById(String userId);

	@Select("select * from tb_relationship where id = #{id}")
	Relationship getRelationshipInfoById(String id);
}
