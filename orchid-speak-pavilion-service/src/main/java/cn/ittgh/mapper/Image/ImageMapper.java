package cn.ittgh.mapper.Image;

import cn.ittgh.pojo.Image.Image;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface ImageMapper {

	@Insert("insert into tb_images(id,user_id,title,file_name,path) values(#{id},#{userId},#{title},#{fileName},#{path})")
	Boolean add(Image image);

	@Select("select * from tb_images where id = #{id}")
	Image getImageById(String id);

	@Select("select * from tb_images where id = #{id} or path = #{id} ")
	Image getImageByIdOrPath(String id);

	@Delete("delete from tb_images where id = #{id}")
	Boolean deleteImageById(String id);


}
