package cn.ittgh.mapper.Calendar;

import cn.ittgh.pojo.Calendar.Calendar;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CalendarMapper {

    @Insert("insert into tb_calendars(user_id,title,cover,calendar_type,remark,times,visible_range)" +
            " values(#{userId},#{title},#{cover},#{calendarType},#{remark},#{times},#{visibleRange})"
    )
    Boolean add(Calendar calendar);

    @Delete("delete from tb_calendars where id = #{id}")
    Boolean deleteById(String id);

    Boolean updateCalendar(Calendar calendar);

    @Select("select * from tb_calendars")
    List<Calendar> getAllCalendar();

    List<Calendar> getCalendarById(String userId);

    @Select("select * from tb_calendars where id = #{id}")
    Calendar getCalendarInfoById(String id);

}
