package cn.ittgh.pojo.Calendar;

import cn.ittgh.utils.EnumsUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Calendar {
	private String id;
	private String userId;

	@Pattern(regexp = "^(\\S| ){3,50}$")
	private String title;

	private String cover;

	@Pattern(regexp = "^(\\S| ){3,255}$")
	private String remark;

	private EnumsUtil.CALENDAR_TYPE calendarType;

	private String visibleRange;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime times;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updateTime;
}