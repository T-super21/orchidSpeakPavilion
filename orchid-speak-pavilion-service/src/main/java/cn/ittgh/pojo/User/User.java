package cn.ittgh.pojo.User;

import cn.ittgh.utils.EnumsUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class User {
	private String id;

	@Pattern(regexp = "^\\S{1,16}$")
	private String userName;

	@Pattern(regexp = "^\\S{3,16}$")
	private String realName;

	private String avatar;

	@Pattern(regexp = "^\\S{11,20}$")
	private String phoneNumber;

	@JsonIgnore
	@Pattern(regexp = "^\\S{5,16}$")
	private String password;

	private EnumsUtil.Sex sex;
	private LocalDateTime birthday;
	private Integer age;

	@Email
	private String email;
	private Boolean isVip;
	private String homePath;
	private EnumsUtil.UserStatus status;
	private Boolean loginStatus;

	@Pattern(regexp = "^\\S{3,200}$")
	private String remark;

	private EnumsUtil.Roles roles;

	private EnumsUtil.PlatformType platformType;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTime;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updateTime;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime lastLoginTime;

	public User(String userName, String password, EnumsUtil.Roles roles, EnumsUtil.PlatformType platformType) {
		this.userName = userName;
		this.password = password;
		this.roles = roles;
		this.platformType = platformType;
	}

	public User(String userName, String password, EnumsUtil.PlatformType platformType) {
		this.userName = userName;
		this.password = password;
		this.platformType = platformType;
	}

	public User(String userName, String phoneNumber, EnumsUtil.PlatformType platformType, EnumsUtil.Roles roles) {
		this.userName = userName;
		this.phoneNumber = phoneNumber;
		this.platformType = platformType;
		this.roles = roles;
	}
}
