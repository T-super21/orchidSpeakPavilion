package cn.ittgh.pojo.Dictionary.GeneralDict;

import cn.ittgh.pojo.Dictionary.Dictionary;
import cn.ittgh.utils.EnumsUtil;

public class GeneralDict {
	public static class ForumTagsDict extends Dictionary<EnumsUtil.FORUM_TAGS, String> {
		public ForumTagsDict() {
			super();
			put(EnumsUtil.FORUM_TAGS.MAKE_FRIENDS, "交友帖");
			put(EnumsUtil.FORUM_TAGS.FEEDBACK, "反馈帖");
			put(EnumsUtil.FORUM_TAGS.SEEK_FOR_HELP, "求助帖");
		}
	}
}

