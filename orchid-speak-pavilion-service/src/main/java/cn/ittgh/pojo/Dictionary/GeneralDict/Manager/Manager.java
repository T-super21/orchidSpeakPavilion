package cn.ittgh.pojo.Dictionary.GeneralDict.Manager;

import cn.ittgh.pojo.Dictionary.GeneralDict.GeneralDict;
import jakarta.validation.constraints.NotNull;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

public class Manager {
	private final GeneralDict.ForumTagsDict forumTagsDict = new GeneralDict.ForumTagsDict();

	public Map getDictionaryByType(
			@RequestParam @NotNull String type) {
		switch (type) {
			case "FORUM_TAGS":
				return forumTagsDict.getMap();
			default:
				throw new IllegalArgumentException("未知的字典类型: " + type);
		}
	}
}
