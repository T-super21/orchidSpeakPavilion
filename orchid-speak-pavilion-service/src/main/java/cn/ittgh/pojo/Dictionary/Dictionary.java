package cn.ittgh.pojo.Dictionary;

import java.util.HashMap;
import java.util.Map;

public class Dictionary<T, V> {
	private Map<T, V> dictionaryMap;

	public Dictionary() {
		this.dictionaryMap = new HashMap<>();
	}

	public void put(T key, V value) {
		dictionaryMap.put(key, value);
	}

	public V get(T key) {
		return dictionaryMap.get(key);
	}

	public Map<T, V> getMap() {
		return dictionaryMap;
	}
}