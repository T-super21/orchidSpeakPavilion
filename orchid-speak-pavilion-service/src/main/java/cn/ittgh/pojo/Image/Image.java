package cn.ittgh.pojo.Image;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Image {
	private String id;
	private String userId;
	private String title;
	private String fileName;
	private String path;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTime;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updateTime;

	public Image(String id, String userId, String originalFilename, String url) {
		this.id = id;
		this.userId = userId;
		this.title = originalFilename;
		this.path = url;
	}

	public Image(String id, String userId, String originalFilename, String fileName, String url) {
		this.id = id;
		this.userId = userId;
		this.title = originalFilename;
		this.fileName = fileName;
		this.path = url;
	}
}
