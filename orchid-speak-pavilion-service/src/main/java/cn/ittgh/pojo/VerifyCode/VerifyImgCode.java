package cn.ittgh.pojo.VerifyCode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

/**
 * 图片验证码
 */
public class VerifyImgCode {
    private String id;
    private String value;
}
