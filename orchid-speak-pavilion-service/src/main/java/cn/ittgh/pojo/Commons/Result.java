package cn.ittgh.pojo.Commons;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Result<T> {
	private Integer code; // 业务状态码 0 -成功 1 -失败
	private String message;
	private T data;

	//success
	public static <T> Result<T> success(T data) {
		return new Result<>(0, "成功", data);
	}

	public static <T> Result<T> success() {
		return new Result<>(0, "成功", null);
	}

	public static <T> Result<T> error(String message) {
		return new Result<>(1, message, null);
	}
}
