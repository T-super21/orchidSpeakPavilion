package cn.ittgh.pojo.Commons;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageBean<T> {
	private Long total; // 总条数
	private List<T> recordedList; // 当前数据集合
}
