package cn.ittgh.pojo.Relationship;

import cn.ittgh.utils.EnumsUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Relationship {
    private String id;

    @NotNull
    private String userId;

    @NotNull
    private String partnerId;

    @JsonProperty("rType")
    private EnumsUtil.RELATIONSHIP rType;

    @Pattern(regexp = "^(\\S| ){0,255}$")
    private String remark;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
}
