package cn.ittgh.pojo.LoveLists;

import cn.ittgh.utils.EnumsUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoveLists {
	private String id;
	private String userId;

	@Pattern(regexp = "^(\\S| ){3,50}$")
	private String title;

	@Pattern(regexp = "^(\\S| ){3,255}$")
	@NotNull
	private String cover;

	@Pattern(regexp = "^(\\S| ){0,255}$")
	private String substance;

	@Pattern(regexp = "^(\\S| ){0,255}$")
	private String remark;

	private String visibleRange;

	private EnumsUtil.LoveListStatus status;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTime;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updateTime;
}
