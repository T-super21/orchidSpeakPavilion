package cn.ittgh.interceptors;

import cn.ittgh.utils.JwtUtil;
import cn.ittgh.utils.ThreadLocalUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Component
public class LoginInterceptor implements HandlerInterceptor {
	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String token = request.getHeader("Authorization");
		// 验证token
		try {
			// redis验证token
			ValueOperations<String, String> redisOperations = stringRedisTemplate.opsForValue();
			if (redisOperations.get("user:token:" + token) == null) {
				throw new RuntimeException();
			}

			Map<String, Object> claims = JwtUtil.parseToken(token);

			ThreadLocalUtil.set(claims);

			return true;
		} catch (Exception e) {
			response.setStatus(401);
			return false;
		}
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
		// 清空ThreadLocal 的数据
		ThreadLocalUtil.remove();
	}
}
