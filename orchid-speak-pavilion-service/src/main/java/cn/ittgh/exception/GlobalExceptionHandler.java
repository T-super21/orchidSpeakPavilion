package cn.ittgh.exception;

import ch.qos.logback.core.util.StringUtil;
import cn.ittgh.pojo.Commons.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
	@ExceptionHandler(Exception.class)
	public <T> Result<T> handlerException(Exception e) {
		e.printStackTrace();
		return Result.error(StringUtil.notNullNorEmpty(e.getMessage()) ? e.getMessage() : "操作失败");
	}
}
