package cn.ittgh.service.Relationship.impl;

import cn.ittgh.mapper.Relationship.RelationshipMapper;
import cn.ittgh.pojo.Relationship.MyRelationship;
import cn.ittgh.pojo.Relationship.Relationship;
import cn.ittgh.pojo.Commons.PageBean;
import cn.ittgh.service.Relationship.RelationshipService;
import cn.ittgh.utils.EnumsUtil;
import cn.ittgh.utils.ThreadLocalUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class RelationshipServiceImpl implements RelationshipService {
	@Autowired
	private RelationshipMapper relationshipMapper;

	@Override
	public PageBean<MyRelationship> getRelationshipById(Integer pageNum, Integer pageSize, String userId) {
		PageBean<MyRelationship> pb = new PageBean<>();
		PageHelper.startPage(pageNum, pageSize);
		List<MyRelationship> myRelationshipList = relationshipMapper.getRelationshipById(userId);
		Page<MyRelationship> p = (Page<MyRelationship>) myRelationshipList;
		pb.setTotal(p.getTotal());
		pb.setRecordedList(p.getResult());
		return pb;
	}

	@Override
	public Boolean addRelationship(Relationship relationship) {
		relationship.setUserId(ThreadLocalUtil.getUserId());
		return relationshipMapper.add(relationship);
	}

	@Override
	public Boolean removeRelationship(String id) {
		return relationshipMapper.deleteById(id);
	}

	@Override
	public Boolean updateRelationship(Relationship relationship) {
		relationship.setUpdateTime(LocalDateTime.now());
		return relationshipMapper.updateRelationship(relationship);
	}

	@Override
	public Relationship getRelationshipInfo(String id) {
		return relationshipMapper.getRelationshipInfoById(id);
	}

	@Override
	public PageBean<MyRelationship> getAllRelationship(Integer pageNum, Integer pageSize) {
		PageBean<MyRelationship> pb = new PageBean<>();
		PageHelper.startPage(pageNum, pageSize);
		List<MyRelationship> myRelationshipList = relationshipMapper.getAllRelationship();
		Page<MyRelationship> p = (Page<MyRelationship>) myRelationshipList;
		pb.setTotal(p.getTotal());
		pb.setRecordedList(p.getResult());
		return pb;
	}

	@Override
	public List<Relationship> getRelationshipByIdAndType(String userId, String partnerId, EnumsUtil.RELATIONSHIP rType) {
		return relationshipMapper.getRelationshipByIdAndType(userId, partnerId, rType);
	}
}
