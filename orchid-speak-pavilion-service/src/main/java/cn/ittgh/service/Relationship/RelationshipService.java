package cn.ittgh.service.Relationship;

import cn.ittgh.pojo.Relationship.MyRelationship;
import cn.ittgh.pojo.Relationship.Relationship;
import cn.ittgh.pojo.Commons.PageBean;
import cn.ittgh.utils.EnumsUtil;

import java.util.List;

public interface RelationshipService {
    PageBean<MyRelationship> getRelationshipById(Integer pageNum, Integer pageSize, String userId);

    Boolean addRelationship(Relationship relationship);

    Boolean removeRelationship(String id);

    Boolean updateRelationship(Relationship relationship);

    Relationship getRelationshipInfo(String id);

    PageBean<MyRelationship> getAllRelationship(Integer pageNum, Integer pageSize);

    List<Relationship> getRelationshipByIdAndType(String userId, String partnerId, EnumsUtil.RELATIONSHIP rType);
}
