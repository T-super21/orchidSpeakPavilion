package cn.ittgh.service.Calendar;

import cn.ittgh.pojo.Commons.PageBean;
import cn.ittgh.pojo.Calendar.Calendar;
import cn.ittgh.utils.EnumsUtil;

public interface CalendarService {
    PageBean<Calendar> getCalendarById(Integer pageNum, Integer pageSize, String userId);

    Boolean addCalendar(Calendar calendar);

    Boolean removeCalendar(String id);

    Boolean updateCalendar(Calendar calendar);

    Calendar getCalendarInfo(String id);

    PageBean<Calendar> getAllCalendar(Integer pageNum, Integer pageSize);
}
