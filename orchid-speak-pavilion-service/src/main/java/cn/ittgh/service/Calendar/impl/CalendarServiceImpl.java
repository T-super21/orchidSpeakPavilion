package cn.ittgh.service.Calendar.impl;

import cn.ittgh.mapper.Calendar.CalendarMapper;
import cn.ittgh.pojo.Commons.PageBean;
import cn.ittgh.pojo.Calendar.Calendar;
import cn.ittgh.service.Calendar.CalendarService;
import cn.ittgh.service.Files.TencentFiles.TencentFilesService;
import cn.ittgh.utils.ThreadLocalUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class CalendarServiceImpl implements CalendarService {
    @Autowired
    private CalendarMapper calendarMapper;

    @Autowired
    TencentFilesService tencentFilesService;

    @Override
    public PageBean<Calendar> getCalendarById(Integer pageNum, Integer pageSize, String userId) {
        PageBean<Calendar> pb = new PageBean<>();
        PageHelper.startPage(pageNum, pageSize);
        List<Calendar> calendarList = calendarMapper.getCalendarById(userId);
        Page<Calendar> p = (Page<Calendar>) calendarList;
        pb.setTotal(p.getTotal());
        pb.setRecordedList(p.getResult());
        return pb;
    }

    @Override
    public Boolean addCalendar(Calendar calendar) {
        calendar.setUserId(ThreadLocalUtil.getUserId());
        return calendarMapper.add(calendar);
    }

    @Override
    public Boolean removeCalendar(String id) {
        // delete love list cover
        tencentFilesService.deleteImage(calendarMapper.getCalendarInfoById(id).getCover());
        return calendarMapper.deleteById(id);
    }

    @Override
    public Boolean updateCalendar(Calendar calendar) {
        calendar.setUpdateTime(LocalDateTime.now());
        return calendarMapper.updateCalendar(calendar);
    }

    @Override
    public Calendar getCalendarInfo(String id) {
        return calendarMapper.getCalendarInfoById(id);
    }

    @Override
    public PageBean<Calendar> getAllCalendar(Integer pageNum, Integer pageSize) {
        PageBean<Calendar> pb = new PageBean<>();
        PageHelper.startPage(pageNum, pageSize);
        List<Calendar> calendarList = calendarMapper.getAllCalendar();
        Page<Calendar> p = (Page<Calendar>) calendarList;
        pb.setTotal(p.getTotal());
        pb.setRecordedList(p.getResult());
        return pb;
    }
}
