package cn.ittgh.service.LoveLists.impl;

import cn.ittgh.mapper.Image.ImageMapper;
import cn.ittgh.mapper.LoveLists.LoveListsMapper;
import cn.ittgh.pojo.Commons.PageBean;
import cn.ittgh.pojo.LoveLists.LoveLists;
import cn.ittgh.service.Files.TencentFiles.TencentFilesService;
import cn.ittgh.service.LoveLists.LoveListsService;
import cn.ittgh.utils.EnumsUtil;
import cn.ittgh.utils.ThreadLocalUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class LoveListsServiceImpl implements LoveListsService {
	@Autowired
	private LoveListsMapper loveListsMapper;

	@Autowired
	TencentFilesService tencentFilesService;

	@Override
	public PageBean<LoveLists> getLoveListsById(Integer pageNum, Integer pageSize, String userId, EnumsUtil.LoveListStatus status) {
		PageBean<LoveLists> pb = new PageBean<>();
		PageHelper.startPage(pageNum, pageSize);
		List<LoveLists> loveListsList = loveListsMapper.getLoveListsById(userId, status);
		Page<LoveLists> p = (Page<LoveLists>) loveListsList;
		pb.setTotal(p.getTotal());
		pb.setRecordedList(p.getResult());
		return pb;
	}

	@Override
	public Boolean addLoveLists(LoveLists loveLists) {
		loveLists.setUserId(ThreadLocalUtil.getUserId());
		return loveListsMapper.add(loveLists);
	}

	@Override
	public Boolean removeLoveLists(String id) {
		// delete love list cover
		tencentFilesService.deleteImage(loveListsMapper.getLoveListsInfoById(id).getCover());
		return loveListsMapper.deleteById(id);
	}

	@Override
	public Boolean updateLoveLists(LoveLists loveLists) {
		loveLists.setUpdateTime(LocalDateTime.now());
		return loveListsMapper.updateLoveLists(loveLists);
	}

	@Override
	public LoveLists getLoveListsInfo(String id) {
		return loveListsMapper.getLoveListsInfoById(id);
	}

	@Override
	public PageBean<LoveLists> getAllLoveLists(Integer pageNum, Integer pageSize) {
		PageBean<LoveLists> pb = new PageBean<>();
		PageHelper.startPage(pageNum, pageSize);
		List<LoveLists> loveListsList = loveListsMapper.getAllLoveLists();
		Page<LoveLists> p = (Page<LoveLists>) loveListsList;
		pb.setTotal(p.getTotal());
		pb.setRecordedList(p.getResult());
		return pb;
	}
}
