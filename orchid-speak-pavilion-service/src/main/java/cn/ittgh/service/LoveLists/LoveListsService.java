package cn.ittgh.service.LoveLists;

import cn.ittgh.pojo.Commons.PageBean;
import cn.ittgh.pojo.LoveLists.LoveLists;
import cn.ittgh.utils.EnumsUtil;

import java.util.List;

public interface LoveListsService {
	PageBean<LoveLists> getLoveListsById(Integer pageNum, Integer pageSize, String userId, EnumsUtil.LoveListStatus status);

	Boolean addLoveLists(LoveLists loveLists);

	Boolean removeLoveLists(String id);

	Boolean updateLoveLists(LoveLists loveLists);

	LoveLists getLoveListsInfo(String id);

	PageBean<LoveLists> getAllLoveLists(Integer pageNum, Integer pageSize);
}
