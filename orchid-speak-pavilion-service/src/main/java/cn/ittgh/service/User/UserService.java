package cn.ittgh.service.User;

import cn.ittgh.pojo.Commons.PageBean;
import cn.ittgh.pojo.User.User;
import cn.ittgh.utils.EnumsUtil;

public interface UserService {
	User findByUserNameAndPlatformType(String userName, EnumsUtil.PlatformType platformType);

	User findByPhoneNumber(String phoneNumber);

	Boolean register(User user);

	Boolean updata(User user);

	User findByUserId(String id);

	Boolean logout(User user);

	PageBean<User> list(Integer pageNum, Integer pageSize, EnumsUtil.Roles roles, EnumsUtil.UserStatus status, EnumsUtil.PlatformType platformType);
}
