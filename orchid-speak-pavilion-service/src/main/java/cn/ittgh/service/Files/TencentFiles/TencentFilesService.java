package cn.ittgh.service.Files.TencentFiles;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface TencentFilesService {
	Map<String, String> uploadFiles(MultipartFile file);

	String deleteImage(String id);
}
