package cn.ittgh.service.VerifyCode;

import cn.ittgh.pojo.VerifyCode.VerifyImgCode;

public interface VerifyImgCodeService {
    public abstract VerifyImgCode generateVerifyImgCode ();
}
