package cn.ittgh.service.Image.impl;

import cn.ittgh.mapper.Image.ImageMapper;
import cn.ittgh.pojo.Image.Image;
import cn.ittgh.service.Image.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ImageServiceImpl implements ImageService {

	@Autowired
	private ImageMapper imageMapper;

	@Override
	public Boolean add(Image image) {
		return imageMapper.add(image);
	}

	@Override
	public Image getImageById(String id) {
		return imageMapper.getImageById(id);
	}
}
