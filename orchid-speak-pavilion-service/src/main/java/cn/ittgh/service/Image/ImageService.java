package cn.ittgh.service.Image;

import cn.ittgh.pojo.Image.Image;

public interface ImageService {

	// add image
	Boolean add(Image image);

	Image getImageById(String id);

}
